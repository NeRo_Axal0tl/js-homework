const p = document.querySelectorAll("p");
p.forEach(item => {
    item.style.backgroundColor = "#ff0000";
});

let optionsList = document.getElementById("optionsList");
console.log(optionsList)

let optionsLisFather = optionsList.parentElement;
console.log(optionsLisFather);

let optionsListChild = optionsList.children;
console.log(optionsListChild);
console.log(typeof optionsListChild);

let testParagraph = document.getElementById("testParagraph");
testParagraph.innerText = "This is a paragraph";

let mainHeader = document.getElementsByClassName("main-header");

for (let i = 0; i < mainHeader.length; i++) {
    let mainHeaderElements = mainHeader[i].querySelectorAll("*");

    for (let j = 0; j < mainHeaderElements.length; j++) {
        mainHeaderElements[j].classList.add("nav-item");
    }

    console.log(mainHeaderElements);
}

allElements = document.body.querySelectorAll("*");

for (let i = 0; i < allElements.length; i++) {
    if (allElements[i].classList.contains("section-title")) {
        allElements[i].classList.remove("section-title");
    }
}