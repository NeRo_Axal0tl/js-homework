let userName;
let userAge; 

do { userName = prompt("What's your name?"); } while (!userName || !userName.trim()); 

do {
  userAge = prompt("How old are you?");
  if (!/^[0-9]+$/.test(userAge)) {
    alert("Invalid input! Please write your real age.");
  }
} while (!/^[0-9]+$/.test(userAge)); 

if (userAge <= 17) {
  alert("You are not allowed to visit this website.");
} else if (userAge >= 18 && userAge <= 22) {
  let youSureQst = confirm("Are you sure?");
  if (youSureQst) {
    alert("Welcome, " + userName + "!");
  } else {
    alert("You are not allowed to visit this website.");
  }
} else if (userAge >= 23) {
  alert("Welcome, " + userName + "!");
}   