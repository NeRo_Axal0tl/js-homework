function createNewUser() {
    let newUser = {
        firstName: prompt("What's your name?"),
        lastName: prompt("What's your surname?"),
        birthday: 0
    };

    let confirmDataNames;

    do {
        while (/\d/.test(newUser.lastName) || /\d/.test(newUser.firstName) || newUser.lastName === "" || newUser.firstName === "") {
            alert("Invalid name format. Please make sure not to include numbers or leave your answer empty");
            newUser.firstName = prompt("What's your name?");
            newUser.lastName = prompt("What's your surname?");
        }
        confirmDataNames = confirm(`You have written "${newUser.firstName}" as your first name and "${newUser.lastName}" as your last name. Are you sure that you want to keep these parameters?`);
        if (confirmDataNames === false) {
            newUser.firstName = prompt("What's your name?");
            newUser.lastName = prompt("What's your surname?");
        }
    } while (confirmDataNames === false);

    let validDate = false;
    while (!validDate) {
        newUser.birthday = prompt("Please enter your date of birth (dd.mm.yyyy):");
        newUser.birthday = new Date(newUser.birthday);
        if (!isNaN(newUser.birthday)) {
            validDate = true;
        } else {
            alert("Invalid date format. Please enter your date of birth in the format dd.mm.yyyy.");
        }
    }

    const today = new Date();
    let ageInYears = today.getFullYear() - newUser.birthday.getFullYear();

    if (today.getMonth() < newUser.birthday.getMonth() ||
        (today.getMonth() === newUser.birthday.getMonth() && today.getDate() < newUser.birthday.getDate())) {
        ageInYears - 1;
    }

    let getAge = ageInYears;
    console.log(`The user is ${getAge} years old.`);

    let upperFirstName = newUser.firstName.toUpperCase();
    let firstLetterOfUpperFirstName = upperFirstName[0];
    let getPasswort = (firstLetterOfUpperFirstName + newUser.lastName.toLowerCase().capitalize() + newUser.birthday.getFullYear());
    console.log(getPasswort);
}

String.prototype.capitalize = function () {
    return this.charAt(0).toUpperCase() + this.slice(1).toLowerCase();
};

createNewUser();