alert("Hey! Give me 2 numbers and an operating sign - I will show you the result ;)");

let firstNumber;
let secondNumber;
let operatingSign;

do {
    firstNumber = prompt("Give me the first number");
    operatingSign = prompt("Give me the operating sign");
    secondNumber = prompt("Give me the second number");

    if (isNaN(firstNumber) || isNaN(secondNumber) || !/^[\+\-\*/]$/.test(operatingSign) || operatingSign.length !== 1) {
        alert("Please, make sure to write everything correctly! Only numbers and an operating sign are needed!");
    }
} while (isNaN(firstNumber) || isNaN(secondNumber) || !/^[\+\-\*/]$/.test(operatingSign) || operatingSign.length !== 1);

function count(firstNumber, operatingSign, secondNumber) {
    if (operatingSign === "+") {
        return firstNumber + secondNumber;
    } else if (operatingSign === "-") {
        return firstNumber - secondNumber;
    } else if (operatingSign === "/") {
        return firstNumber / secondNumber;
    } else {
        return firstNumber * secondNumber; 
    }
}

const result = count(Number(firstNumber), operatingSign, Number(secondNumber));
console.log(`The result is: ${result}`);
