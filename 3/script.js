let n = prompt("Give me a number and I'll tell you all the numbers between 0 and yours :)")

function findNumers() {
    let numbers = [];

    for (let i = 1; i <= n; i++) {
        if (i % 5 === 0) {
            numbers.push(i)
        }
    }
    if (numbers.length === 0) {
        console.log("No numbers :(")
    } else console.log(numbers);

}

findNumers();



// function findPrimesInRange(m, n) {
//     let primes = [];

//     for (let i = m; i <= n; i++) {

//         let isPrime = true;
//         for (let j = 2; j <= Math.sqrt(i); j++) {
//             if (i % j === 0) {
//                 isPrime = false;
//                 break;
//             }
//         }

//         if (isPrime && i > 1) {
//             primes.push(i);
//         }
//     }

//     return primes;
// }

// alert("Hello! Give me two different numbers and I will tell you which prime numbers are hiding between these :)");
// let m = prompt("Write first number");
// let n = prompt("Write second number");

// while (!/^[0-9]+$/.test(m) || !/^[0-9]+$/.test(n) || m > n || m === null || n === null) {
//     alert("Your first number must be greater or equal to your second number. Please don't leave empty space in the answer!")
//     m = prompt("Write first number");
//     n = prompt("Write second number");
// }

// let primes = findPrimesInRange(parseInt(m), parseInt(n));

// console.log(primes);