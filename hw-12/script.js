let buttonLetters = document.querySelectorAll(".btn");
console.log(buttonLetters);

document.body.addEventListener("keyup", (e) => {
    console.log(e["key"].toUpperCase());
    for (let i = 0; i < buttonLetters.length; i++) {
        if (buttonLetters[i].classList.contains("blue-letter")) {
            buttonLetters[i].classList.remove("blue-letter")    
            buttonLetters[i].classList.add("black-letter")    
        }
        if (e["key"].toUpperCase() === buttonLetters[i].innerText) {
            buttonLetters[i].classList.add("blue-letter");
        }
    }
});