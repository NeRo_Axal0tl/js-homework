function createList(array, parent = document.body) {
    const ul = document.createElement("ul");
    parent.appendChild(ul);

    array.forEach((item) => {
        const li = document.createElement("li");
        ul.appendChild(li);

        if (Array.isArray(item)) {
            createList(item, ul);
        } else {
            li.textContent = item;
        }
    });

    let count = 3;
    const timerId = setInterval(() => {
        if (count === 0) {
            clearInterval(timerId);
            parent.removeChild(ul);
            console.log("Page cleared!");
        } else {
            console.log(`Page will be cleared in ${count} seconds`);
            count--;
        }
    }, 1000);
}

const array = ["Kharkiv", "Kiev", ["Borispol", "Irpin"], "Odessa", "Lviv", "Dnieper"];

createList(array);